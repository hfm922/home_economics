/* jshint esversion: 6 */

import Vue from 'vue'

import App from './App.vue'

import './assets/common.less'

import dataV from '@jiaminghi/data-view'

import BootstrapVue from 'bootstrap-vue'

import echarts from 'echarts'

Vue.prototype.$echarts = echarts

Vue.config.productionTip = false

Vue.use(dataV)
Vue.use(BootstrapVue)

new Vue({
  render: h => h(App)
}).$mount('#app')
